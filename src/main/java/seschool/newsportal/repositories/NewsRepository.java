package seschool.newsportal.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import seschool.newsportal.beans.New;

import java.util.List;

public interface NewsRepository extends PagingAndSortingRepository<New, Long> {
    /**
     * Найти новость по флагу
      * @param show-флаг
     * @return
     */
    List<New> findByShowOnMain(boolean show);

    /**
     * Найти новость по флагу с постраничкой
     * @param show-флаг
     * @return
     */
   Page<New> findByShowOnMain(boolean show, Pageable pegeable);

   @Query("FROM news WHERE title LIKE %:query% or text LIKE %:query%")
   List<New> search(@Param("query") String query);
}
