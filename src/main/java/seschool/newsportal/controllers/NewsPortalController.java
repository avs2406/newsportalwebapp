package seschool.newsportal.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import seschool.newsportal.beans.New;
import seschool.newsportal.beans.NewsList;
import seschool.newsportal.repositories.AuthorsRepository;
import seschool.newsportal.repositories.RolesRepository;
import seschool.newsportal.repositories.UsersRepository;
import seschool.newsportal.security.Role;
import seschool.newsportal.security.User;
import seschool.newsportal.security.UserService;

import java.security.Principal;
import java.util.Optional;

@Controller
public class NewsPortalController {
	@Autowired
	private NewsList newsList;
	@Autowired
	private UserService userService;


	@Autowired
	private AuthorsRepository authorsRepository;

	@RequestMapping(value = "/test/")
	public String test(ModelMap model) {


		return "list";
	}

	@RequestMapping(value = "/")
	public String showMainPage(ModelMap model, Principal user) {

		this.setupHeader(model,user);
		model.addAttribute("newsList", this.newsList.getNewsForMain(8));
		return "main";
	}

	@RequestMapping(value = "/list/")
	public String newsList(ModelMap model, @RequestParam(name = "page", defaultValue = "1") Integer pageNum,
						   Principal user) {
		this.setupHeader(model,user);
		//новостей на странице
		int pageSize = 5;

		NewsList newsList = this.newsList;
		model.addAttribute("newsList", newsList.getList(pageNum, pageSize));

		Integer prevPage, nextPage;
		//посчитать страницы для кнопок "вперед" и "назад"

		if (pageNum < 2) {
			prevPage = null;
		} else {
			prevPage = pageNum - 1;
		}

		//всего страниц
		int pageCount = newsList.getCount() / pageSize + 1;
		//если последняя, то null
		if (pageNum >= pageCount) {
			nextPage = null;
		} else {
			nextPage = pageNum + 1;
		}
		model.addAttribute("prevPage", prevPage);
		model.addAttribute("nextPage", nextPage);

		return "list";
	}

	@RequestMapping(value = "/list/{id}")
    public String detailNews(ModelMap model, @PathVariable Integer id, Principal userPrincipal) {

	   this.setupHeader(model,userPrincipal);
		model.addAttribute("editLink",false);
	   User user =(User) model.get("currentUser");
		if (user!=null) {
			for(Role role:user.getRoles()){
				if(user.getRoles().equals("ROLE_EDITOR")){
					model.addAttribute("editLink", true);
				}
			}
		}
		NewsList list = this.newsList;
		model.addAttribute("detailNew", list.getById(id));

		return "detail";
	}

	@RequestMapping(value = "/search")
	public String searchResult(ModelMap model, @RequestParam(name = "q", defaultValue = "") String query,
							   Principal user) {

		this.setupHeader(model,user);
		//добавит результаты поиска в модель
		model.addAttribute("query", query);
		model.addAttribute("searchResult", this.newsList.search(query));

		return "search";
	}

	@RequestMapping(value = "/saveNew/", method = RequestMethod.POST)
	public String addNew(@RequestParam(defaultValue = "") String title,
						 @RequestParam(defaultValue = "") String text,
						 @RequestParam(defaultValue = "") String date,
	                     @RequestParam(defaultValue = "") String imgUrl,
						 @RequestParam(defaultValue = "0") Integer showOnMain,
						 @RequestParam(defaultValue = "0") Long id,
						 ModelMap model, Principal user)

	{

		this.setupHeader(model,user);

		User currentUser=(User)model.get("currentUser");

		New newItem = new New();
		newItem.setTitle(title);
		newItem.setText(text);
		if (currentUser!=null) {
			newItem.setWriter(currentUser.getName());
		}
		newItem.setDate(date);
		newItem.setPicture(imgUrl);
		newItem.setShowOnMain(false);
		if(id>0) {
			newItem.setId(id);
		}
		if (showOnMain==1){
			newItem.setShowOnMain(true);
		}


		this.newsList.saveNew(newItem);

		return "redirect:/list/"+newItem.getId();
	}

	@RequestMapping(value = "/game/{name}/")
	public String showGame(ModelMap model,
						   @PathVariable String name,
						   @RequestParam (required = false,defaultValue = "all") String platform) {
		model.addAttribute("name", name);
		model.addAttribute("platform", platform);

		return "test";
	}

	/**
	 * показать форму для редактирования / добавления новостей
	 * @param model
	 * @return
	 */
	@RequestMapping(value = {"/newsform/","/newsform/{id}/"})
	public String showNewsForm(ModelMap model, @PathVariable Optional<Integer> id,
							   Principal user){

		this.setupHeader(model,user);

		if (id.isPresent()) {
			New newToUpdate = this.newsList.getById(id.get());
			if (newToUpdate != null) {
				model.addAttribute("newToUpdate", newToUpdate);
			}
			else {
				return "redirect:/newsform/";
			}
		}
	else {
			model.addAttribute("newToUpdate", null);
		}
		return "newsform";
	}

	@RequestMapping("deleteNew/{id}/")
	public String deleteNew (ModelMap model,@PathVariable Long id,
							 Principal user)	{
		this.setupHeader(model,user);
		boolean success=newsList.deleteNew(id);
		model.addAttribute("success",success);

		return "deleteResult";
	}

	@RequestMapping(value = "/adduser/", method = RequestMethod.POST)
	public String registerUser(User newUser){

		if (newUser.getPassword().equals(newUser.getPasswordConfirm())){
			this.userService.saveUser(newUser);
		}
		else{
			return "redirect:/register/";
		}

		return "redirect:/";
	}

	@RequestMapping(value = "/register/")
	public String showRegisterForm(ModelMap model) {


		return "register";
	}

	@RequestMapping(value = "/login/")
	public String showLoginForm(ModelMap model) {


		return "login";
	}

	/**
	 * Служебный метод для настройки шапки
	 * @param model
	 */
	private void setupHeader(ModelMap model, Principal userPrincipal){
		if (userPrincipal!=null){
			User user=(User)this.userService.loadUserByUsername(userPrincipal.getName());
			model.addAttribute("currentUser", user);
		}

	}

}


