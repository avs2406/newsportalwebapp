package seschool.newsportal.beans;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import seschool.newsportal.repositories.NewsRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Service
public class NewsList {

    @Autowired
    private NewsRepository repository;

    /**
     * вернуть список новостей
     *
     * @return список новостей
     */

    public List<New> getList(int page, int limit) {

      return repository.findAll(PageRequest.of(page-1,limit)).getContent();

    }

    public List<New> getNewsForMain(Integer limit){
        return repository.findByShowOnMain(true, PageRequest.of(0, limit)).getContent();
    }

    public int getCount() {
        return (int) repository.count();
    }

    public New getById(int id) {
        //Найти
        Optional<New> result = repository.findById((long) id);
       //если нашел
       if(result.isPresent()){
           //вернуть новость
           return result.get();
       }
       return null;
    }

    //вернуть список всех новостей, содержащих слово query в заголовке или в тексте
    public List<New> search(String query) {

        return repository.search(query);
    }

    public Long saveNew(New newItem){

        return this.repository.save(newItem).getId();
    }

    /**
     * вернуть список промотируемых новостей
     * @return
     */

    public boolean deleteNew (Long id){
        try {
            repository.deleteById(id);
        }
        catch (Exception e){
            return false;
        }

        return true;
    }

}
